package csp_etud;

import java.io.BufferedReader;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.util.ArrayList;

public class ConstraintArithmetique extends Constraint {
	protected String expression;

	public ConstraintArithmetique(ArrayList<String> vars) {
		super(vars);
		// TODO Auto-generated constructor stub
	}

	public ConstraintArithmetique(ArrayList<String> vars, String name) {
		super(vars, name);
		// TODO Auto-generated constructor stub
	}

	public ConstraintArithmetique(BufferedReader in) throws Exception {
        super(in);
        this.expression = in.readLine();
    }

	@Override
	public boolean nonCoherente(Assignment a) {
		
		
		String buffer=expression;
		
        for(String var : this.getVars()){
            buffer = buffer.replace(var, a.get(var).toString());
        }

        
        boolean result = false;
        
        try {
        	ScriptEngineManager mgr = new ScriptEngineManager();
            ScriptEngine engine = mgr.getEngineByName("JavaScript");
            result = (boolean) engine.eval(buffer);
        }
        catch (ScriptException e) { 
        	System.out.println(buffer);
        	System.err.println("Erreur : Expression arithmétique mal formée");
        }
		return !result;
	}

}
