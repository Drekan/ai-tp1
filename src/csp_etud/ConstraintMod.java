package csp_etud;

import java.io.BufferedReader;
import java.util.ArrayList;

public class ConstraintMod extends Constraint {
	private ArrayList<String> partieGauche;
	private String reste;

	public ConstraintMod(ArrayList<String> vars) {
		super(vars);
		this.partieGauche=new ArrayList<String>();
		reste="";
		// TODO Auto-generated constructor stub
	}

	public ConstraintMod(ArrayList<String> vars, String name) {
		super(vars, name);
		this.partieGauche=new ArrayList<String>();
		reste="";
	}

	public ConstraintMod(BufferedReader in) throws Exception {
		super(in);
		this.partieGauche=new ArrayList<String>();
		reste="";
		for(String s : in.readLine().split(";")) {
			if(!this.varList.contains(s)) {
				System.out.println(s);
				System.err.println("Erreur : l'équation contient des variables inexistantes : ");
			}
			
			this.partieGauche.add(s);
		}
		this.reste=in.readLine();
		if(!this.varList.contains(reste)) {
			System.out.println(reste);
			System.err.println("Erreur : l'équation contient des variables inexistantes : ");
		}
	}

	@Override
	public boolean nonCoherente(Assignment a) {
		int sommeGauche=0;
		for(int i=0;i<partieGauche.size();i++) {
			sommeGauche+=Integer.valueOf((String)a.get(partieGauche.get(i)));
		}
		boolean test=(sommeGauche%10)!=Integer.valueOf((String)a.get(reste));
		System.out.println("mod : "+!test);
		return test;
	}
	
	public String toString() {
		String resultat=super.toString()+" (";
		for(int i=0;i<this.partieGauche.size();i++) {
			resultat+="+"+this.partieGauche.get(i)+" ";
		}
		resultat+=")%10 = "+this.reste;
		return resultat;
	}

}
