package csp_etud;

import java.io.BufferedReader;
import java.util.ArrayList;

public class ConstraintQuo extends Constraint {
	private ArrayList<String> partieGauche;
	private String dividende;

	public ConstraintQuo(ArrayList<String> vars) {
		super(vars);
		this.partieGauche=new ArrayList<String>();
		dividende="";
		// TODO Auto-generated constructor stub
	}

	public ConstraintQuo(ArrayList<String> vars, String name) {
		super(vars, name);
		this.partieGauche=new ArrayList<String>();
		dividende="";
	}

	public ConstraintQuo(BufferedReader in) throws Exception {
		super(in);
		this.partieGauche=new ArrayList<String>();
		dividende="";
		for(String s : in.readLine().split(";")) {
			if(!this.varList.contains(s)) {
				System.out.println(s);
				System.err.println("Erreur : l'équation contient des variables inexistantes : ");
			}
			
			this.partieGauche.add(s);
		}
		this.dividende=in.readLine();
		if(!this.varList.contains(dividende)) {
			System.out.println(dividende);
			System.err.println("Erreur : l'équation contient des variables inexistantes : ");
		}
	}

	@Override
	public boolean nonCoherente(Assignment a) {
		int sommeGauche = 0;
		
		for(int i = 0;i<partieGauche.size();i++) {
			sommeGauche += Integer.valueOf((String)a.get(partieGauche.get(i)));
		}
		
		boolean test = (sommeGauche>=10?1:0)!=(int)Integer.valueOf((String)a.get(dividende));

		return test;
	}
	
	public String toString() {
		String resultat=super.toString()+" (";
		for(int i=0;i<this.partieGauche.size();i++) {
			resultat+="+"+this.partieGauche.get(i)+" ";
		}
		resultat+=")/10 = "+this.dividende;
		return resultat;
	}

}
