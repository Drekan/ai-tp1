package csp_etud;

import java.io.BufferedReader;
import java.util.HashMap;
import java.util.ArrayList;

public class ConstraintEquation extends Constraint {
	
	private HashMap<String,Integer> signe;
	private ArrayList<String> partieGauche;
	private ArrayList<String> partieDroite;

	public ConstraintEquation(ArrayList<String> vars) {
		super(vars);
		this.partieDroite=new ArrayList<String>();
		this.partieGauche=new ArrayList<String>();
		this.signe=new HashMap<String,Integer>();

	}

	public ConstraintEquation(ArrayList<String> vars, String name) {
		super(vars, name);
		this.partieDroite=new ArrayList<String>();
		this.partieGauche=new ArrayList<String>();
		this.signe=new HashMap<String,Integer>();
	}

	public ConstraintEquation(BufferedReader in) throws Exception {
		
		super(in);
		this.partieDroite=new ArrayList<String>();
		this.partieGauche=new ArrayList<String>();
		this.signe=new HashMap<String,Integer>();
		char signe='0';
		String var="";
		for(String s : in.readLine().split(";")){
			if(s.charAt(0)=='+' || s.charAt(0)=='-') {
				signe=s.charAt(0);
			}
			else {
				System.err.println("Erreur : '+' ou '-' attendu");
			}
			
			var="";
			for(int i=1;i<s.length();i++) {
				var+=s.charAt(i);
			}
			if(!this.varList.contains(var)) {
				System.out.println(var);
				System.err.println("Erreur : l'équation contient des variables inexistantes : ");
			}
			
			this.partieGauche.add(var);
			this.signe.put(var,(signe=='+'?1:-1));
			
		}
		
		for(String s : in.readLine().split(";")){
			if(s.charAt(0)=='+' || s.charAt(0)=='-') {
				signe=s.charAt(0);
			}
			else {
				System.err.println("Erreur : '+' ou '-' attendu");
			}
			
			var="";
			for(int i=1;i<s.length();i++) {
				var+=s.charAt(i);
			}
			if(!this.varList.contains(var)) {
				System.out.println(var);
				System.err.println("Erreur : l'équation contient des variables inexistantes : ");
			}
			this.partieDroite.add(var);
			this.signe.put(var,(signe=='+'?1:-1));
			
		}
	}

	@Override
	public boolean nonCoherente(Assignment a) {
		int sommeGauche=0,sommeDroite=0;
		for(int i=0;i<partieGauche.size();i++) {
			sommeGauche+=Integer.valueOf((String)a.get(partieGauche.get(i)))*(signe.get(partieGauche.get(i)));
		}
		for(int i=0;i<partieDroite.size();i++) {
			sommeDroite+=Integer.valueOf((String)a.get(partieDroite.get(i)))*(signe.get(partieDroite.get(i)));
		}
		boolean test=((sommeGauche)%10)!=sommeDroite;
		return test;
	}
	
	public String toString() {
		String resultat=super.toString()+" (";
		for(int i=0;i<this.partieGauche.size();i++) {
			resultat+=" +"+this.partieGauche.get(i)+" ";
		}
		resultat+=")%10 = ";
		for(int i=0;i<this.partieDroite.size();i++) {
			resultat+=this.partieDroite.get(i)+" ";
		}
		resultat+=")";
		return resultat;
	}

}
