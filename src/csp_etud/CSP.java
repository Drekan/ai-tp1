package csp_etud;

import java.util.ArrayList;

/**
 * Solveur : permet de résoudre un problème de contrainte par Backtrack : 
 * 	Calcul d'une solution, 
 * 	Calcul de toutes les solutions
 *
 */
public class CSP {

		private Network network;					// le réseau à résoudre
		private Assignment assignment;				// l'assignation courante (résultat de searchSolution)
		private ArrayList<Assignment> solutions; 	// les solutions du réseau (résultat de searchAllSolutions)
		private int cptr;							// le compteur de noeuds explorés

		
		/**
		 * Crée un problème de résolution de contraintes pour un réseau donné
		 * 
		 * @param r le réseau de contraintes à résoudre
		 */
		public CSP(Network r) {
			network = r;
			solutions = new ArrayList<Assignment>();
			assignment = new Assignment();

		}
	       
		
		/********************** BACKTRACK UNE SOLUTION *******************************************/
		 
		/**
		 * Cherche une solution au réseau de contraintes
		 * 
		 * @return une assignation solution du réseau, ou null si pas de solution
		 */
		public Assignment searchSolution() {
		    // Mise en oeuvre d’eventuels pre-traitements :
		    //         - simplification du reseau de contraintes
		    //	       - calcul d’un ordre statique sur l’affectation des variables
		    //	       - …
		    assignment.clear();            // on part d’une assignation courante vide
		    cptr = 1 ;                      // on compte le noeud associe a l’assignation vide
		    Assignment sol = backtrack();   // appel a backtrack
		    System.out.println("première solution trouvée : "+ sol);
		    System.out.println("Nombre de noeuds générés : "+cptr);
		    
		    return sol;
		}

		/* La methode bactrack ci-dessous travaille directement sur l'attribut assignment. 
		 * On peut aussi choisir de ne pas utiliser cet attribut et de créer plutot un objet Assignment local à searchSolution : 
		 * dans ce cas il faut le passer en parametre de backtrack
		 */
		/**
		 * Exécute l'algorithme de backtrack à la recherche d'une solution en étendant l'assignation courante
		 * Utilise l'attribut assignment
		 * @return la prochaine solution ou null si pas de nouvelle solution
		 */
		
		private Assignment backtrack() {   // utilise l'attribut assignment pour l'assignation courante au lieu de la passer en parametre
		    if(this.assignment.getVars().size()==this.network.getVarNumber()){
		    	//System.out.println(this.assignment);
		    	return this.assignment.clone();
		    }
		    String x=chooseVar();
		    //if(this.cptr%100==0)System.out.println(cptr);
		    for(Object i : this.network.getDom(x)){
		    	
		    	this.assignment.put(x, i);
//		    	System.out.println(x+"->"+i);
//		    	System.out.println(this.assignment);
		    	if(this.consistantOpt(x)){
		    		Assignment bt=backtrack();
		    		this.cptr++;
		    		if(bt!=null){
		    			return bt;
		    		}
		    	}
		    }
		    this.assignment.remove(x);
		    return null;
		}

    
     		/**
		 * Retourne la prochaine variable à assigner étant donné assignment (qui doit contenir la solution partielle courante)
		 *  
		 * @return une variable non encore assignée
		 */
		private String chooseVar() {
			
		    int counter=this.assignment.size();
		    if(counter<this.network.getVars().size()){
		    	return this.network.getVars().get(counter);
		    }
		    return null;
		}
				
	       
		/**
		 * Teste si l'assignation courante stokée dans l'attribut assignment est consistante, c'est à dire qu'elle
		 * ne viole aucune contrainte.
		 * 
		 * @param lastAssignedVar la variable que l'on vient d'assigner à cette étape
		 * @return vrai ssi l'assignment courante ne viole aucune contrainte
		 */
		private boolean consistant(String lastAssignedVar) {
			for(Constraint c : this.network.getConstraints(lastAssignedVar)){
				
				if(c.violation(assignment)){
					return false;
				}
			}
			return true;
		}
		
		private boolean consistantOpt(String lastAssignedVar) {
			
			for(Constraint c : this.network.getConstraints(lastAssignedVar)){
				
				if(c.violationOpt(assignment)){
					return false;
				}
			}
			return true;
		}
		

		/********************** BACKTRACK TOUTES SOLUTIONS *******************************************/
		
		/**
		 * Calcule toutes les solutions au réseau de contraintes
		 * 
		 * @return la liste des assignations solution
		 * 
		 */
		public ArrayList<Assignment> searchAllSolutions(){
			cptr=1;
			solutions.clear(); 
			assignment.clear();
			backtrackAll();
			System.out.println("Nombre de solutions trouvées : "+solutions.size());
			System.out.println("Toutes les solutions trouvées : "+solutions);
		    System.out.println("Nombre de noeuds générés : "+cptr);
			return solutions;
		}
		
		/**
		 * Exécute l'algorithme de backtrack à la recherche de toutes les solutions
		 * étendant l'assignation courante
		 * 
		 */
		private void backtrackAll() {   // utilise l'attribut assignment pour l'assignation courante au lieu de la passer en parametre
		    if(this.assignment.getVars().size()==this.network.getVarNumber()){
		    	this.solutions.add(this.assignment.clone());
		    }else {
			    String x=chooseVar();
			    if(x!=null) {
				    for(Object i : this.network.getDom(x)){
				    	this.assignment.put(x, i);
				    	if(this.consistantOpt(x)){
				    		backtrackAll();
				    		this.cptr++;
				    	}
				    }
			    }
			    this.assignment.remove(x);
		    }
		}

    
    		/**
		 * Fixe un ordre de prise en compte des valeurs d'un domaine
		 * 
		 * @param values une liste de valeurs
		 * @return une liste de valeurs
		 */
		private ArrayList<Object> tri(ArrayList<Object> values) {
			return values; // donc en l'état n'est pas d'une grande utilité ! 
		}
		 		
}
