package csp_etud;

import java.io.BufferedReader;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineFactory;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

public class Application {
	
	static protected String filename;
	
	public static void main(String[] args) {
		
		String filename="reseauContrainte";
		
		try{
			
			BufferedReader br = new BufferedReader(new FileReader(filename));
			
			Network nw = new Network(br);
			
			CSP solveur = new CSP(nw);
			
			System.out.println(nw);
			
			solveur.searchSolution();
			
			ArrayList<Assignment> retour=solveur.searchAllSolutions();
			//System.out.println(retour.size()+" solutions trouvées");

			br.close();
			
		}catch(Exception e){		
			e.printStackTrace();
		}

		

	}

}
