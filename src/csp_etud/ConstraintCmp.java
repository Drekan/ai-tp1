package csp_etud;

import java.io.BufferedReader;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineFactory;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import java.util.ArrayList;
import java.util.List;

public class ConstraintCmp extends Constraint {
	protected String expression;

	
	public ConstraintCmp(ArrayList<String> vars) {
		super(vars);
		// TODO Auto-generated constructor stub
	}

	
	public ConstraintCmp(ArrayList<String> vars, String name) {
		super(vars, name);
		// TODO Auto-generated constructor stub
	}

	
	public ConstraintCmp(BufferedReader in) throws Exception {
		super(in);
		
		if(this.varList.size()>1) {
			
			System.err.println("Contrainte cmp : Erreur de syntaxe");
		}
		
		this.expression = in.readLine();
	}

	
	@Override
	public boolean nonCoherente(Assignment a) {
		
		boolean coherent = false;
		
		Object valeur = a.get(this.varList.get(0));
		
		String fullExpression = valeur.toString() + this.expression;
		
		
		try {
			ScriptEngineManager scriptManager=new ScriptEngineManager();

			ScriptEngine engine=scriptManager.getEngineByName("JavaScript");
			
			coherent=(boolean)engine.eval(fullExpression);
		}
		catch(ScriptException e) {
			System.err.println("Problème dans "+fullExpression);
		}
		
		return !coherent;
	}
	
	@Override
	public String toString() {
		return super.toString() + " (Cmp)";
	}

}
