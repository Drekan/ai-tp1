package csp_etud;

import java.io.BufferedReader;
import java.util.ArrayList;

public class ConstraintEq extends Constraint {

	public ConstraintEq(ArrayList<String> vars) {
		super(vars);
	}

	public ConstraintEq(ArrayList<String> vars, String name) {
		super(vars, name);
	}

	public ConstraintEq(BufferedReader in) throws Exception {
		super(in);
	}


	@Override
	public boolean nonCoherente(Assignment a) {
		
		ArrayList<Object> tuple = new ArrayList<Object>();
		
		for(int i = 0;i<this.varList.size();i++) {
			tuple.add(a.get(this.varList.get(i)));
		}
		
		Object value=tuple.get(0);
		
		for(int i = 0;i<tuple.size();i++) {
			if(tuple.get(i)!=value) {
				return false;
			}
		}
		
		return true;
	}
	
	@Override
	public String toString() {
		return super.toString() + " (AllSame)";
	}
	
	public boolean inclusionOpt(Assignment a) {
		
		int commonVar = 0;
		
		for(String s : this.varList) {
			if(a.containsKey(s)) commonVar++;
			if(commonVar>=2) break;
		}
		return commonVar >= 2;
	}
	
	public boolean coherenteOpt(Assignment a) {
		
		ArrayList<Object> tuple=new ArrayList<Object>();
		
		for(int i=0;i<this.varList.size();i++) {
			if(a.containsKey(this.varList.get(i))) {
				tuple.add(a.get(this.varList.get(i)));
			}else {
				tuple.add(null);
			}
		}
		
		
		for(int i = 0;i<tuple.size();i++) {
			for(int j =0;j<tuple.size();j++) {
				if(tuple.get(i)!=null && tuple.get(j)!=null)
					if(i != j && !tuple.get(i).equals(tuple.get(j))) {
						return false;
					}
			}
		}
		return true;
	}

}
