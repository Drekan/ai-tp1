package csp_etud;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;


/* SEGURA Bastien 2150 1647
 * tache1() : csp1.txt
 * tache2() : csp2.txt
 * tache3() : 5reines.txt
 * tache4() : 8reines.txt
 * tache5() : colore.txt
 * tache6() : 5reinesExp.txt
 * tache7() : cryptoMoney.txt
 * tache8() : cryptoMoneyIntension.txt
 */




public class Demo {
	
	public static void tache1(){
		String filename="csp1.txt";
		System.out.println("problème csp1");
		
		try{
			
			BufferedReader br = new BufferedReader(new FileReader(filename));
			
			Network nw = new Network(br);
			
			CSP solveur = new CSP(nw);
			
			System.out.println("search sol : ");
			
			solveur.searchSolution();
			
			System.out.println("\nsearchAllSol : ");
			
			solveur.searchAllSolutions();
			
			
			//System.out.println(retour.size()+" solutions trouvées");

			br.close();
			
		}catch(Exception e){		
			e.printStackTrace();
		}
	}
	
	public static void tache2(){
		String filename="csp2.txt";
		System.out.println("problème csp2");
		
		try{
			
			BufferedReader br = new BufferedReader(new FileReader(filename));
			
			Network nw = new Network(br);
			
			CSP solveur = new CSP(nw);
			
			System.out.println("search sol : ");
			
			solveur.searchSolution();
			
			System.out.println("\nsearchAllSol : ");
			
			solveur.searchAllSolutions();
			
			
			//System.out.println(retour.size()+" solutions trouvées");

			br.close();
			
		}catch(Exception e){		
			e.printStackTrace();
		}
	}
	
	public static void tache3(){
		String filename="5reines.txt";
		System.out.println("problème 5 reines");
		
		try{
			
			BufferedReader br = new BufferedReader(new FileReader(filename));
			
			Network nw = new Network(br);
			
			CSP solveur = new CSP(nw);
			
			System.out.println("search sol : ");
			
			solveur.searchSolution();
			
			System.out.println("\nsearchAllSol : ");
			
			solveur.searchAllSolutions();
			
			
			//System.out.println(retour.size()+" solutions trouvées");

			br.close();
			
		}catch(Exception e){		
			e.printStackTrace();
		}
	}
	
	public static void tache4(){
		String filename="8reines.txt";
		System.out.println("Problème 8reines");
		
		try{
			
			BufferedReader br = new BufferedReader(new FileReader(filename));
			
			Network nw = new Network(br);
			
			CSP solveur = new CSP(nw);
			
			System.out.println("search sol : ");
			
			solveur.searchSolution();
			
			System.out.println("\nsearchAllSol : ");
			
			solveur.searchAllSolutions();
			
			
			//System.out.println(retour.size()+" solutions trouvées");

			br.close();
			
		}catch(Exception e){		
			e.printStackTrace();
		}
	}
	
	public static void tache5(){
		String filename="colore.txt";
		System.out.println("Problème colore");
		
		try{
			
			BufferedReader br = new BufferedReader(new FileReader(filename));
			
			Network nw = new Network(br);
			
			CSP solveur = new CSP(nw);
			
			System.out.println("search sol : ");
			
			solveur.searchSolution();
			
			System.out.println("\nsearchAllSol : ");
			
			solveur.searchAllSolutions();
			
			
			//System.out.println(retour.size()+" solutions trouvées");

			br.close();
			
		}catch(Exception e){		
			e.printStackTrace();
		}
	}
	
	public static void tache6(){
		String filename="5reinesExp.txt";
		System.out.println("Problème 5reinesExp");
		
		try{
			
			BufferedReader br = new BufferedReader(new FileReader(filename));
			
			Network nw = new Network(br);
			
			CSP solveur = new CSP(nw);
			
			System.out.println("search sol : ");
			
			solveur.searchSolution();
			
			System.out.println("\nsearchAllSol : ");
			
			solveur.searchAllSolutions();
			
			
			//System.out.println(retour.size()+" solutions trouvées");

			br.close();
			
		}catch(Exception e){		
			e.printStackTrace();
		}
	}
	
	public static void tache7(){
		String filename="cryptoMoney.txt";
		System.out.println("Problème cryptoMoney");
		
		try{
			
			BufferedReader br = new BufferedReader(new FileReader(filename));
			
			Network nw = new Network(br);
			
			CSP solveur = new CSP(nw);
			
			System.out.println("search sol : ");
			
			solveur.searchSolution();
			
			System.out.println("\nsearchAllSol : ");
			
			solveur.searchAllSolutions();
			
			
			//System.out.println(retour.size()+" solutions trouvées");

			br.close();
			
		}catch(Exception e){		
			e.printStackTrace();
		}
	}
	
	public static void tache8(){
		String filename="cryptoMoneyIntension.txt";
		System.out.println("Problème CryptoMoneyIntension");
		
		try{
			
			BufferedReader br = new BufferedReader(new FileReader(filename));
			
			Network nw = new Network(br);
			
			CSP solveur = new CSP(nw);
			
			System.out.println("search sol : ");
			
			solveur.searchSolution();
			
//			System.out.println("\nsearchAllSol : ");
//			
//			solveur.searchAllSolutions();
			
			

			br.close();
			
		}catch(Exception e){		
			e.printStackTrace();
		}
	}

	
	public static void main(String[] args) {
		tache1();
		System.out.println();System.out.println();
		tache2();
		System.out.println();System.out.println();
		tache3();
		System.out.println();System.out.println();
		tache4();
		System.out.println();System.out.println();
		tache5();
		System.out.println();System.out.println();
		tache6();
		System.out.println();System.out.println();
		tache7();
		System.out.println();System.out.println();
		tache8();
	}
}
