package csp_etud;

import java.io.BufferedReader;
import java.util.ArrayList;

public class ConstraintDif extends Constraint {

	public ConstraintDif(ArrayList<String> vars) {
		super(vars);
		// TODO Auto-generated constructor stub
	}

	public ConstraintDif(ArrayList<String> vars, String name) {
		super(vars, name);
		// TODO Auto-generated constructor stub
	}

	public ConstraintDif(BufferedReader in) throws Exception {
		super(in);
	}


	@Override
	public boolean nonCoherente(Assignment a) {
		
		ArrayList<Object> tuple = new ArrayList<Object>();
		
		
		//création du tuple A[P(c)]
		for(int i = 0;i<this.varList.size();i++) {
			tuple.add(a.get(this.varList.get(i)));
		}
		
		
		//test si A[P(c)] est un tuple de la contrainte
		for(int i = 0;i<tuple.size();i++) {
			
			for(int j = 0;j<tuple.size();j++) {
				if(i != j && tuple.get(i).equals(tuple.get(j))) {	
					return true;	
				}
			}
		}
		
		
		return false;
	}
	
	@Override
	public String toString() {
		return super.toString() + " (AllDiff)";
	}
	
	public boolean inclusionOpt(Assignment a) {
		int commonVar = 0;
		for(String s : this.varList) {
			if(a.containsKey(s)) commonVar++;
			if(commonVar >= 2) break;
			
		}
			
		return commonVar>=2;
	}
	
	public boolean coherenteOpt(Assignment a) {
		
		ArrayList<Object> tuple = new ArrayList<Object>();
		
		//construction du tuple A[P(c)] (A étant potentiellement partielle, certaines valeurs peuvent être nulles
		for(int i=0;i<this.varList.size();i++) {	
			if(a.containsKey(this.varList.get(i))) {
				tuple.add(a.get(this.varList.get(i)));
				
			}else{
				tuple.add(null);
				
			}
		}
		
		//check que toutes les valeurs sont bien égales
		for(int i = 0;i<tuple.size();i++) {
			
			for(int j =0;j<tuple.size();j++) {
				if(tuple.get(i)!=null && tuple.get(j)!=null)
					if(i != j && tuple.get(i).equals(tuple.get(j))) {
						return false;
					}
			}
			
		}
		
		
		return true;
	}
}
